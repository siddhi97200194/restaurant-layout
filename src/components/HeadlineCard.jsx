import React from 'react'

const HeadlineCard = () => {
  return (
    <div className='max-w-[1640px] mx-auto p-4 py-12 grid md:grid-cols-3 gap-6'>
        {/*Card*/}
        <div className='rounded-xl relative'>
            {/*Overlay*/}
            <div className='absolute w-full h-full bg-black/30 rounded-xl text-white'>
              <p className='font-bold text-2xl px-2 pt-4'>Sunny Sides Up</p>
              <p className='px-2'>Through 8/26</p>
              <button className='border-white bg-white text-black mx-2 absolute bottom-4'>Order Now</button>
            </div>
            <img className='max-h-[1640px] md:max-h-[200px] w-full object-cover rounded-xl' src="https://images.pexels.com/photos/139746/pexels-photo-139746.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="/" />
        </div>

         {/*Card*/}
         <div className='rounded-xl relative'>
            {/*Overlay*/}
            <div className='absolute w-full h-full bg-black/30 rounded-xl text-white'>
              <p className='font-bold text-2xl px-2 pt-4'>New Items</p>
              <p className='px-2'>Added Daily</p>
              <button className='border-white bg-white text-black mx-2 absolute bottom-4'>Order Now</button>
            </div>
            <img className='max-h-[1640px] md:max-h-[200px] w-full object-cover rounded-xl' src="https://images.pexels.com/photos/2456435/pexels-photo-2456435.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="/" />
        </div>

         {/*Card*/}
         <div className='rounded-xl relative'>
            {/*Overlay*/}
            <div className='absolute w-full h-full bg-black/30 rounded-xl text-white'>
              <p className='font-bold text-2xl px-2 pt-4'>Now Delivering Desserts</p>
              <p className='px-2'>Grab some sweet</p>
              <button className='border-white bg-white text-black mx-2 absolute bottom-4'>Order Now</button>
            </div>
            <img className='max-h-[1640px] md:max-h-[200px] w-full object-cover rounded-xl' src="https://images.pexels.com/photos/291528/pexels-photo-291528.jpeg?auto=compress&cs=tinysrgb&w=600" alt="/" />
        </div>
    </div>
  )
}

export default HeadlineCard